<?php

use Faker\Generator as Faker;

$factory->define(App\Link::class, function (Faker $faker) {
    return [
        'title' => substr($faker->sentence(2), 0, -1),
        'uid' => mt_rand(1,5),
        'url' => $faker->url,
        'description' => $faker->paragraph,
    ];
});
