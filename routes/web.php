<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Auth::routes();

Route::get('/', 'LinksController@index');
Route::get('/dashboard', 'LinksController@index');
Route::get('/show/{id}', 'LinksController@show');

Route::get('/submit', 'LinksController@create');
Route::post('/submit', 'LinksController@store');
Route::get('/delete/{id}', 'LinksController@destroy');
Route::get('/edit/{id}', 'LinksController@edit');
Route::post('/edit/{id}', 'LinksController@update');

Route::get('/admin', 'AdminController@index');
Route::get('/admin/edit/{id}', 'AdminController@edit');
Route::get('/admin/delete/{id}', 'AdminController@destroy');
Route::post('/admin/edit/{id}', 'AdminController@update');