# Links Project
This project started from a tutorial on the web that I found to help me learn more about Laravel, I have changed some things along the way and plan to carry on. My ambitions for this project are to add an up/downvote system and a comment section.

I have used Faker to add fake data to the database to allow me to easily work with my application without having to manually add data.
I have used PHPUnit and learned the usefulness of Unit Testing!

## Installation

`cp .env.example .env` to ensure the environment file has been copied. Change the database details here.

Please run `php artisan key:generate` to generate your application key for authentication.

Then to migrate and seed the database: `php artisan migrate:fresh --seed`

Hopefully all done!