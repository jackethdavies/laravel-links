@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ $post->title }} <small><a href="{{ $post->url }}" target="_blank">View URL</a></small></div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">{{ $post->description }}</li>
                    <li class="list-group-item">Posted by <strong>{{ $user->name }}</strong> at <strong>{{ $post->created_at }}</strong></li>
                    @if($current == $post->uid || $myUser->level > 1)
                        <li class="list-group-item">
                            <a href="/edit/{{ $post->id }}"><button class="btn btn-primary">Update</button></a>
                            <a href="/delete/{{ $post->id }}"><button class="btn btn-danger">Delete</button></a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
