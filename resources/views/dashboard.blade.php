@extends('layouts.app')

@section('content')
<div class="container">

    @if ($user->level > 1)
        <div class="alert alert-success" role="alert">
        Welcome, {{ $user->name }}. You are a {{ \App\Http\Controllers\UserController::formatStaff($user->level) }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Latest Posts</div>

                <div class="card-body">
                    <ul class="list-group">
                        @foreach($links as $link)
                            <li class="list-group-item"><a href="/show/{{ $link->id }}">{{ $link->title }}</a> <small><a href="{{ $link->url }}" target="_blank" rel="noopener noreferrer">{{ $link->url }}</a></small></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection