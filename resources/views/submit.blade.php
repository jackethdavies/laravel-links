@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Submit Link</div>

                <div class="card-body">
                    <form action="/submit" method="post">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                Please fix the following errors
                            </div>
                        @endif

                        {!! csrf_field() !!}
                        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title">Title</label>                
                            <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                            @if($errors->has('title'))
                                <span class="help-block">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('url') ? ' has-error' : '' }}">
                            <label for="url">URL</label>                
                            <input type="text" class="form-control" id="url" name="url" placeholder="URL">
                            @if($errors->has('url'))
                                <span class="help-block">{{ $errors->first('url') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('url') ? ' has-error' : '' }}">
                            <label for="description">Description</label>                
                            <textarea name="description" id="description" placeholder="Descripton" class="form-control"></textarea>
                            @if($errors->has('description'))
                                <span class="help-block">{{ $errors->first('description') }}</span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection