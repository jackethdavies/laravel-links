@extends('layouts.app')

@section('content')
<div class="container">

    @if ($user->level > 1)
        <div class="alert alert-success" role="alert">
        Welcome, {{ $user->name }}. You are a {{ \App\Http\Controllers\UserController::formatStaff($user->level) }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Administration Panel</div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Username</th>
                                <th scope="col">User Level</th>
                                <th scope="col">Posts</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($allUsers as $oneUser)
                                <tr>
                                    <th scope="row">{{ $oneUser->name }}</th>
                                    <td>{{ \App\Http\Controllers\UserController::formatStaff($oneUser->level) }}</td>
                                    <td>{{ $oneUser->posts }}</td>
                                    <td>
                                        <a href="/admin/edit/{{ $oneUser->id }}"><button class="btn btn-primary">Edit User</button></a>
                                        @if ($oneUser->level < 2)
                                            <a href="/admin/delete/{{ $oneUser->id }}"><button class="btn btn-danger">Delete User</button></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection