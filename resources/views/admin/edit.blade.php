@extends('layouts.app')

@section('content')
<div class="container">

    @if ($user->level > 1)
        <div class="alert alert-success" role="alert">
            Welcome, {{ $user->name }}. You are a {{ \App\Http\Controllers\UserController::formatStaff($user->level) }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Editing <strong>{{ $foundUser->name }}</strong></div>

                <div class="card-body">
                <form action="/admin/edit/{{ $foundUser->id }}" method="post">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                Please fix the following errors
                            </div>
                        @endif

                        {!! csrf_field() !!}
                        <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username">Username</label>                
                            <input type="text" class="form-control" id="username" name="username" placeholder="username" value="{{ $foundUser->name }}">
                            @if($errors->has('username'))
                                <span class="help-block">{{ $errors->first('username') }}</span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Edit User</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection