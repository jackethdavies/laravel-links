<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public static function formatStaff($level) {
        switch($level) {
            case 1:
                $pos = "Normal User";
                return $pos;
            case 2:
                $pos = "Moderator";
                return $pos;
            case 3:
                $pos = "Administrator";
                return $pos;
        }
    }
}
