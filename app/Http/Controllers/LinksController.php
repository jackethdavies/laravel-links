<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LinksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = \App\Link::orderBy('id', 'desc')->get();
        return view('dashboard')->with([
            'links' => $links,
            'user' => \Auth::user()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('submit')->with([
            'user' => \Auth::user()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|max:255',
            'url' => 'required|url|max:255',
            'description' => 'required|max:255',
        ]);
    
    
        $link = new \App\Link;
        $link->title = $data['title'];
        $link->uid = \Auth::user()->id;
        $link->url = $data['url'];
        $link->description = $data['description'];
    
        $link->save();

        \Auth::user()->increment('posts');
    
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $findPost = \App\Link::findOrFail($id);
        $getUser = \App\User::find($findPost->uid);
        $currentId = \Auth::user()->id;
        return view('show')->with([
            "post" => $findPost,
            "user" => $getUser,
            "current" => $currentId,
            "myUser" => \Auth::user()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currentId = \Auth::user()->id;
        $getPost = \App\Link::findOrFail($id);
        if ($currentId != $getPost->uid && \Auth::user()->level < 2) {
            return redirect('/');
        } else {
            return view('edit')->with([
                "post" =>  $getPost,
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'title' => 'required|max:255',
            'url' => 'required|url|max:255',
            'description' => 'required|max:255',
        ]);
        $findPost = \App\Link::findOrFail($id);
        if (\Auth::user()->id != $findPost->uid && \Auth::user()->level < 2) {
            return redirect('/');
        } else {
            $findPost->title = $data['title'];
            $findPost->url = $data['url'];
            $findPost->description = $data['description'];
            $findPost->save();
            return redirect('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentId = \Auth::user()->id;
        $getPost = \App\Link::findOrFail($id);
        if ($getPost->uid != $currentId && \Auth::user()->level < 2) {
            return redirect('/');
        } else {
            $getPost->delete();
            return redirect('/');
        }
    }
}
